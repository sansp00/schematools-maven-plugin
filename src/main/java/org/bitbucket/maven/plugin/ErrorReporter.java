package org.bitbucket.maven.plugin;

import org.apache.maven.plugin.logging.Log;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ErrorReporter implements ErrorHandler {

	private final Log log;

	public ErrorReporter(Log o) {
		this.log = o;
	}


	public void warning(SAXParseException e) throws SAXException {
		log.warn(e);
	}

	public void error(SAXParseException e) throws SAXException {
		log.error(e);
	}

	public void fatalError(SAXParseException e) throws SAXException {
		log.error(e);
	}
}
