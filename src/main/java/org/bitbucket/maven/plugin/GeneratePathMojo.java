package org.bitbucket.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.DirectoryScanner;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.parser.XSOMParser;
import com.sun.xml.xsom.util.DomAnnotationParserFactory;

/**
 * Generates the Path values of a xml schema file
 */
@Mojo(name = "generatePath", requiresProject = true)
public class GeneratePathMojo extends AbstractMojo {
	/**
	 * The source directory containing *.xsd schema and *.xjb binding files.
	 */
	@Parameter(required = true)
	protected File schemaDirectory;

	/**
	 * A list of regular expression file search patterns to specify the schemas
	 * to be processed. Searching is based from the root of schemaDirectory. If
	 * this is not set then all .xsd files in schemaDirectory will be processed.
	 * Default is *.xsd.
	 * <p/>
	 * todo In maven2 2 how to set default for String[]? expression="*.xsd"
	 */
	@Parameter(required = true)
	protected String[] includeSchemas;

	/**
	 * A list of regular expression file search patterns to specify the schemas
	 * to be excluded from the includeSchemas list. Searching is based from the
	 * root of schemaDirectory.
	 */
	@Parameter(required = false)
	protected String[] excludeSchemas;

	/**
	 * Generated code will be written under this directory. If you specify
	 * target="doe/ray" and generatePackage="org.here", then files are generated
	 * to doe/ray/org/here.
	 */
	@Parameter(defaultValue = "/target/xpath")
	protected File generateDirectory;

	/**
	 * The file name to output
	 * 
	 */
	@Parameter(defaultValue = ".txt")
	private String generateExtension;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.maven.plugin.Mojo#execute()
	 */
	public void execute() throws MojoExecutionException {
		// Create the destination path if it does not exist.
		if (generateDirectory != null && !generateDirectory.exists()) {
			generateDirectory.mkdirs();
		}

		// Get list of schemas
		String[] schemaFilenames = getFiles(schemaDirectory, includeSchemas,
				excludeSchemas);
		try {
			processSchemaFile(schemaFilenames);
		} catch (SAXException e) {
			getLog().error(e);
		} catch (IOException e) {
			getLog().error(e);
		}
	}

	/**
	 * Parses a schema file
	 * 
	 * @param aFileToParse
	 * @param aErrorReporter
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 */
	protected XSSchemaSet parseSchemaFile(String aFileToParse,
			ErrorReporter aErrorReporter) throws SAXException, IOException {
		XSOMParser reader = new XSOMParser();
		// set an error handler so that you can receive error messages
		reader.setErrorHandler(aErrorReporter);
		// DomAnnotationParserFactory is a convenient default to use
		reader.setAnnotationParser(new DomAnnotationParserFactory());

		reader.parse(new File(aFileToParse));

		XSSchemaSet xss = reader.getResult();

		if (xss == null) {
			throw new IllegalStateException("Schema set is empty");
		}

		return xss;
	}

	/**
	 * Process a list of schemas
	 * 
	 * @param schemaFilenames
	 * @throws SAXException
	 * @throws IOException
	 */
	protected void processSchemaFile(String[] schemaFilenames)
			throws SAXException, IOException {
		for (int i = 0; i < schemaFilenames.length; i++) {
			String schemaFilename = schemaFilenames[i];
			getLog().info("Processing " + schemaFilename);
			ErrorReporter errorReporter = new ErrorReporter(getLog());
			XSSchemaSet xss = parseSchemaFile(schemaFilename, errorReporter);
			String basename = org.apache.commons.io.FilenameUtils
					.getBaseName(schemaFilename);

			File fileToGenerate = new File(generateDirectory, basename
					+ generateExtension);
			PrintStream outputStream = new PrintStream(fileToGenerate);
			getLog().info("Generating " + fileToGenerate.toString());

			Document document = null;
			try {
				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				// document = db.parse(new File(fileToGenerate.toString()));
				DOMImplementation impl = db.getDOMImplementation();
				// Document.
				document = impl.createDocument(null,
						DocumentBuilderTagEnum.ROOT.getTag(), null);

			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			SchemaVisitorDocumenter documenter = new SchemaVisitorDocumenter(
					document);
			documenter.visit(xss);

			try {
				TransformerFactory tFactory = TransformerFactory.newInstance();
				Transformer transformer = tFactory.newTransformer();
				DOMSource source = new DOMSource(document);
				StreamResult result = new StreamResult(outputStream);
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.transform(source, result);
			} catch (TransformerException e) {
				e.printStackTrace();
			}

			outputStream.flush();
			outputStream.close();
		}
	}

	/**
	 * Go through each directory and get the files for this dir.
	 * 
	 * @param searchRootDirectory
	 * @param includePatterns
	 * @param excludePatterns
	 * @return list of schemas found based on pattern criteria.
	 */
	private String[] getFiles(File searchRootDirectory,
			String[] includePatterns, String[] excludePatterns) {
		DirectoryScanner directoryScanner = new DirectoryScanner();
		directoryScanner.setBasedir(searchRootDirectory);

		directoryScanner.setIncludes(includePatterns);
		directoryScanner.setExcludes(excludePatterns);

		directoryScanner.scan();
		String[] files = directoryScanner.getIncludedFiles();

		// Prepend with base dir.
		for (int i = 0; i < files.length; i++) {
			files[i] = directoryScanner.getBasedir() + File.separator
					+ files[i];
		}

		return files;
	}

	/**
	 * Sets the generation path
	 * 
	 * @param directory
	 */
	public void setGenerateDirectory(File directory) {
		this.generateDirectory = directory;
	}

	/**
	 * Sets the generation file extension
	 * 
	 * @param extension
	 */
	public void setGenerateExtension(String extension) {
		this.generateExtension = extension;
	}
}